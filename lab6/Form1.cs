﻿using lab3;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab6
{
    public partial class Form1 : Form
    {
        private List<IntegerArray> arrays;
        private bool EditFlag = false;
        private IntegerArray CurrentArray;

        public Form1()
        {
            InitializeComponent();
            arrays = new List<IntegerArray>();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitializeDataGridInput();

            ToggleButton(buttonCreate);
        }

        #region Инициализация DataGridView
        /// <summary>
        /// Инициализирует DataGridView для вывода краткой информации по всем массивам в коллекции.
        /// </summary>
        private void InitializeDataGridView()
        {
            dataGridView1.RowsAdded -= HandlerAddedRow;
            dataGridView1.Columns.Clear();

            var column1 = new DataGridViewColumn();
            column1.HeaderText = "ID";
            column1.Name = "ID";
            column1.ReadOnly = true;
            column1.CellTemplate = new DataGridViewTextBoxCell();

            dataGridView1.Columns.Add(column1);
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.MultiSelect = false;

            if (arrays.Count < 1) return;

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = arrays;
            dataGridView1.Columns["N"].HeaderText = "Количество элементов";

            for (int i = 0; i < arrays.Count; i++)
            {
                dataGridView1.Rows[i].Cells["ID"].Value = i.ToString();
            }
        }

        /// <summary>
        /// Инициализирует DataGridView для заполнения массива.
        /// </summary>
        private void InitializeDataGridInput()
        {
            dataGridView1.RowsAdded -= HandlerAddedRow;
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.ReadOnly = false;

            var column1 = new DataGridViewColumn();
            column1.CellTemplate = new DataGridViewTextBoxCell();
            column1.ReadOnly = true;
            column1.HeaderText = "№";
            column1.Name = "ID";

            var column2 = new DataGridViewColumn();
            column2.CellTemplate = new DataGridViewTextBoxCell();
            column2.ReadOnly = false;
            column2.HeaderText = "Значение";
            column2.Name = "Value";

            dataGridView1.Columns.Add(column1);
            dataGridView1.Columns.Add(column2);
            dataGridView1.AllowUserToAddRows = true;
            dataGridView1.AllowUserToDeleteRows = true;
            dataGridView1.MultiSelect = true;
            dataGridView1.RowsAdded += HandlerAddedRow;
        }

        private void InitializeDataGridEdit(int[] array)
        {
            dataGridView1.RowsAdded -= HandlerAddedRow;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.MultiSelect = false;
            for (int i = 0; i < array.Length; i++)
                dataGridView1.Rows.Add(new string[] { $"{i + 1}", $"{array[i]}" });
        }
        #endregion

        #region Инициализация рабочей области формы
        /// <summary>
        /// Инициализация рабочей области формы при СОЗДАНИИ массива.
        /// </summary>
        private void InitializeFormCreate()
        {
            EditFlag = false;
            InitializeDataGridInput();
        }
        /// <summary>
        /// Инициализация рабочей области формы при ПРОСМОТРЕ массивов.
        /// </summary>
        private void InitializeFormView()
        {
            EditFlag = true;
            InitializeDataGridView();
            if (buttonCreate.Visible) ToggleButton(buttonCreate);
            if (buttonEdit.Visible) ToggleButton(buttonEdit);
        }
        /// <summary>
        /// Инициализация рабочей области формы при РЕДАКТИРОВАНИИ массива.
        /// </summary>
        private void InitializeFormEdit()
        {
            EditFlag = false;
            InitializeDataGridInput();
            InitializeDataGridEdit(CurrentArray.IntArray);
            if (!buttonEdit.Visible) ToggleButton(buttonEdit);
            if (buttonCreate.Visible) ToggleButton(buttonCreate);
        }
        #endregion

        #region ToggleButton, GetDataInput, HandlerAddedRow
        private void ToggleButton(Button button) => button.Visible = !button.Visible;

        /// <summary>
        /// Добавляет ID к строкам при добавилении массива.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandlerAddedRow(object sender, EventArgs e)
        {
            int ID = dataGridView1.CurrentCell.RowIndex;
            dataGridView1.Rows[ID].Cells[0].Value = (++ID).ToString();
        }

        /// <summary>
        /// Вовзращает List, содержащий массивы типа int.
        /// При возникновении ошибки преобразования возвращет null.
        /// </summary>
        /// <returns></returns>
        private List<int> GetDataInput()
        {
            List<int> arr = new List<int>();

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (row.IsNewRow) continue;
                int val;

                if (row.Cells["Value"].Value == null)
                {
                    arr = null;
                    MessageBox.Show($"Строка { row.Cells["ID"].Value}: введите значение.");
                    break;
                }

                if (!Int32.TryParse(row.Cells["value"].Value.ToString(), out val))
                {
                    arr = null;
                    MessageBox.Show($"Строка {row.Cells["ID"].Value}: введите целое число.");
                    break;
                }

                arr.Add(val);
            }
            return arr;
        }
        #endregion

        #region Кнопка - Создать
        private void buttonCreate_Click(object sender, EventArgs e)
        {
            List<int> array = GetDataInput();
            if (array == null) return;
            if (array.Count < 1)
            {
                MessageBox.Show("Введите хотя бы 1 число.");
                return;
            }
            MessageBox.Show("Массив успешно создан.");
            arrays.Add(new IntegerArray(array.ToArray()));

            InitializeFormCreate();
        }

        #endregion

        #region Кнопка - Редактировать
        private void buttonEdit_Click(object sender, EventArgs e)
        {
            List<int> array = GetDataInput();

            if (array == null) return;

            CurrentArray.IntArray = array.ToArray();
            MessageBox.Show("Массив успешно Редактирован.");

            InitializeFormEdit();
        }
        #endregion

        #region Меню - Коллекция - Выход
        private void ToolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Меню - Коллекция - Создать
        private void ToolStripMenuItemCreate_Click(object sender, EventArgs e)
        {
            if (!buttonCreate.Visible) ToggleButton(buttonCreate);
            if (buttonEdit.Visible) ToggleButton(buttonEdit);
            InitializeFormCreate();
        }
        #endregion    

        #region Меню - Коллекция - Просмотреть
        private void ToolStripMenuItemView_Click(object sender, EventArgs e)
        {
            if (arrays.Count == 0)
            {
                MessageBox.Show("Сначала надо добавить массивы.");
                return;
            }
            InitializeFormView();
        }
        #endregion

        #region Меню - Коллекция - Редактировать
        private void ToolStripMenuItemEdit_Click(object sender, EventArgs e)
        {
            if (!EditFlag)
            {
                MessageBox.Show("Вы можете использовать эту функцию только при \"просмотре\" всех массивов.");
                return;
            }

            DataGridViewCell cell = dataGridView1.CurrentCell;

            if (cell == null)
            {
                MessageBox.Show("Добавьте данные или выберите ячейку.");
                return;
            }

            int index = cell.RowIndex;
            int id;

            if (!Int32.TryParse(dataGridView1.Rows[index].Cells["ID"].Value.ToString(), out id))
            {
                MessageBox.Show("Не удалось получить ID элемента.");
                return;
            }

            CurrentArray = arrays[id];
            InitializeFormEdit();
        }
        #endregion

        #region Меню - Коллекция - Сохранить
        private void SaveData(Stream file)
        {
            foreach (IntegerArray item in arrays)
            {
                byte[] array = System.Text.Encoding.Default.GetBytes(item.ToString() + "\n");
                file.Write(array, 0, array.Length);
            }
        }

        /// <summary>
        /// Процедура сохранения данных. Если массивов нет, то сохранение не производится.
        /// </summary>
        private void SaveProces()
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string path = saveFileDialog1.FileName;
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    SaveData(myStream);
                    File.SetAttributes(path, FileAttributes.ReadOnly);
                    myStream.Close();
                }
            }
        }
        private void ToolStripMenuItemSave_Click(object sender, EventArgs e)
        {
            if (arrays.Count == 0)
            {
                MessageBox.Show("Нет данных для сохранения.");
                return;
            }
            SaveProces();
        }
        #endregion

        #region Меню - Коллекция - Загрузить
        private int[] ParseLine(string Line)
        {
            string[] numbers = Line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            List<int> array = new List<int>();

            for (int i = 0; i < numbers.Length; i++)
            {
                if (!Int32.TryParse(numbers[i], out int val))
                    return null;

                array.Add(val);
            }

            return array.ToArray();
        }
        private void ReadData(StreamReader file)
        {
            arrays.Clear();
            string data;
            int[] arr;

            while ((data = file.ReadLine()) != null)
            {
                arr = ParseLine(data);
                if (arr == null)
                {
                    MessageBox.Show("Не удалось обработать файл, возможно он был повреждён.");
                    break;
                }
                arrays.Add(new IntegerArray(arr));
            }
        }
        private void ToolStripMenuItemLoad_Click(object sender, EventArgs e)
        {
            if (arrays.Count > 0)
            {
                DialogResult res = MessageBox.Show("Сохранить данные?", "Сохранение", MessageBoxButtons.YesNoCancel);
                if (res == DialogResult.Cancel)
                {
                    return;
                }
                if (res == DialogResult.Yes)
                {
                    SaveProces();
                }
            }

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    string filePath = openFileDialog.FileName;

                    //Read the contents of the file into a stream
                    Stream fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {

                        ReadData(reader);
                        reader.Close();
                    }
                }
            }

            InitializeFormView();
        }
        #endregion

        #region Меню - Работа - Сортировка
        private void ToolStripMenuItemSort_Click(object sender, EventArgs e)
        {
            if (arrays.Count == 0)
            {
                MessageBox.Show("Нет данных для сортировки.");
                return;
            }

            arrays.Sort(new SortIntegerArray());
            arrays.Reverse();
            InitializeFormView();
        }
        #endregion

        #region Меню - Поиск
        private IntegerArray SearchArray()
        {
            IntegerArray array = null;
            foreach (IntegerArray item in arrays)
                if (!item)
                {
                    if ((object)array == null)
                        array = item;
                    else
                    {
                        if (item.GetSum() > array.GetSum())
                            array = item;
                    }
                }
            return array;
        }

        private void ToolStripMenuItemSearch_Click(object sender, EventArgs e)
        {
            CurrentArray = SearchArray();
            if ((object)CurrentArray == null)
            {
                MessageBox.Show("Не было найдено ни одного массива.");
                return;
            }
            InitializeFormEdit();
        }
        #endregion

        #region Меню - Задание
        private IntegerArray SearchMaxArray()
        {
            IntegerArray array = null;
            foreach (IntegerArray item in arrays)
                if ((object)array == null)
                    array = item;
                else
                {
                    if (item.N > array.N)
                        array = item;
                }
            return array;
        }
        private void TaskToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IntegerArray array = SearchMaxArray();
            if ((object)array == null)
            {
                MessageBox.Show("Не было найдено ни одного массива.");
                return;
            }
            int count = 0;
            foreach (IntegerArray item in arrays)
                if (item.N == array.N)
                {
                    ++count;
                    Array.Reverse(item.IntArray);
                }
            MessageBox.Show($"Было изменено массивов: {count}");
            return;
        }
        #endregion

        /// <summary>
        /// Сортирует список по убыванию сумм масивов.
        /// </summary>
        class SortIntegerArray : IComparer<IntegerArray>
        {
            public int Compare(IntegerArray arr1, IntegerArray arr2)
            {
                if (arr1 < arr2)
                    return -1;
                else if (arr1 == arr2)
                    return 0;
                else
                    return 1;
            }
        }
    }

}
