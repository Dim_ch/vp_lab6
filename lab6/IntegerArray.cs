﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class IntegerArray
    {
        int[] intArray;
        int n;
        //Свойства.
        public int[] IntArray
        {
            get { return intArray; }
            set
            {
                if (value.Length == N)
                {
                    for (int i = 0; i < value.Length; i++)
                        intArray[i] = value[i];
                }
                else
                    throw new ArgumentOutOfRangeException();
            }
        }

        public int N
        {
            get { return n; }
            set
            {
                if (value > 0)
                {
                    n = value;
                }
                else
                    throw new ArgumentOutOfRangeException();
            }
        }
        //Конструкторы.
        public IntegerArray(int n)
        {
            N = n;
            intArray = new int[n];
        }

        public IntegerArray(int[] arr)
        {
            N = arr.Length;
            intArray = arr;
        }
        //Методы.

        public int GetSum()
        {
            int sum = 0;
            foreach (int i in IntArray)
                sum += i;
            return sum;
        }

        public void SortArr() => Array.Sort(IntArray);
        //Перегрузка операторов.
        public static IntegerArray operator ++(IntegerArray arr)
        {
            int len = arr.n;
            for (int i = 0; i < len; i++)
                arr.IntArray[i]++;
            return arr;
        }

        public static IntegerArray operator --(IntegerArray arr)
        {
            int len = arr.n;
            for (int i = 0; i < len; i++)
                arr.IntArray[i]--;
            return arr;
        }

        public static bool operator !(IntegerArray arr)
        {
            //Возвращает true, если массив упорядочен по убыванию.
            bool flag = true;
            for (int i = 0; i < arr.n - 1; i++)
                if (arr.IntArray[i] < arr.IntArray[i + 1])
                {
                    flag = false;
                    break;
                }
            return flag;

        }

        public static bool operator >(IntegerArray arr1, IntegerArray arr2) => arr1.GetSum() > arr2.GetSum();
        public static bool operator <(IntegerArray arr1, IntegerArray arr2) => arr1.GetSum() > arr2.GetSum();
        public static bool operator >=(IntegerArray arr1, IntegerArray arr2) => arr1.GetSum() >= arr2.GetSum();
        public static bool operator <=(IntegerArray arr1, IntegerArray arr2) => arr1.GetSum() <= arr2.GetSum();
        public static bool operator ==(IntegerArray arr1, IntegerArray arr2) => arr1.GetSum() == arr2.GetSum();
        public static bool operator !=(IntegerArray arr1, IntegerArray arr2) => arr1.GetSum() != arr2.GetSum();

        public static IntegerArray operator *(IntegerArray arr1, IntegerArray arr2)
        {
            if (arr1.n != arr2.n)
                throw new ArgumentOutOfRangeException();
            else
            {
                IntegerArray obj = new IntegerArray(arr1.n);
                for (int i = 0; i < arr1.n; i++)
                    obj.IntArray[i] = arr1.IntArray[i] * arr2.IntArray[i];
                return obj;
            }

        }
        //Переопределение методов.
        /// <summary>
        /// Возвращает массив, каждый элемент которого записан через пробел.
        /// </summary>
        /// <returns>Массив в виде строки.</returns>
        public override string ToString()
        {
            string str = "";
            for (int i = 0; i < N - 1; i++)
                str += IntArray[i].ToString() + " ";
            str += IntArray[N - 1];
            return str;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
